
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require("clean-webpack-plugin");

const path = require('path');

module.exports = {

	
	devServer: {
		host: '127.0.0.1',
		port: 7180
	},

	entry: './src/main.js',
	output: {
		filename: 'bundle.[hash].js'
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				use: ['vue-loader']
			},
			{
				test: /\.css$/,
				use: [{
					loader: 'vue-style-loader'
				}, {
					loader: 'css-loader'
				}, {
					loader: 'px2rem-loader',
					// options here
					options: {
						remUnit: 50,
						remPrecision: 8
					}
				}]// 直接插入到style标签中

			},
			{
				test: /\.(eot|svg|png|jpg|ttf|woff|woff2)(\?\S*)?$/,
				loader: 'file-loader'
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({ // 此插件必须位于插件数组的首位
			template: './src/index.html',// 指定模板html文件
			filename: './index.html',// 输出的html文件名称
			favicon: path.resolve('./src/assets/img/favicon.png'), //标签页图片
			inject: true,
		}),
		new VueLoaderPlugin(),
		new CleanWebpackPlugin()
	],
	// externals: {
	// 	'AMap': 'window.AMap'
	// }
};
