
// Vue
import Vue from 'vue';

import myCharts from './assets/js/myCharts.js';
Vue.use(myCharts);

// vuex
import Vuex from 'vuex';
Vue.use(Vuex);
const store = new Vuex.Store({
	state: {
		theme: "light", //主题
		inited: false,
		config: {
			webTitle: '辰小白', //系统名称
			isCollapse: false, //默认折叠垂直菜单栏
			isHeaderHide: true, //头部是否可隐藏
			isVertical: true, //默认使用垂直菜单
			isXs: true, //判断是否隐藏横向菜单和面包屑，请不要改动
			informNum: 12 //新通知数
		},
		currentUser: {},
		// mapInfo: {
		// 	center: [113.293359, 22.80524],
		// 	zoom: 14
		// }
		tobMenu: [
			{
				name: "首页",
				path: "/home"
			}
		]
	},
	mutations: {
		setInited: function (state, data) {
			state.inited = data;
		},
		setConfig: function (state, data) {
			state.config = data;
		},
		setCurrentUser: function (state, data) {
			state.currentUser = data;
		}
	}
});


// 全局样式
import './assets/css/app.css';
// 图标样式
import './assets/icon/iconfont.css';
// 浅色样式
import './assets/css/light.css';
// 深色样式
import './assets/css/dark.css';
//使用Animate.css动画库
import './assets/css/animate.css';

import 'lib-flexible/flexible'

// element-ui
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
Vue.use(ElementUI);

// NProgress 顶端进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({
	easing: 'ease',  // 动画方式    
	speed: 500,  // 递增进度条的速度    
	showSpinner: false, // 是否显示加载ico    
	trickleSpeed: 100, // 自动递增间隔    
	minimum: 0.3, // 初始化时的最小百分比
	// parent:".el-main" //parent来设置进度条的父容器，默认父容器是body
})







// axios
import axios from 'axios';
axios.defaults.baseURL = '/noobAdmin/';// 配置统一根路径
Vue.prototype.$axios = axios;

// 请求拦截器
axios.interceptors.request.use(data => {
	//获取token加入消息头
	// data.headers.token = sessionStorage.getItem('token');
	showFullScreenLoading() //显示loading
	return data
}, (error) => {
	return Promise.reject(error)
})
// --响应异常拦截
// import { Message } from 'element-ui';
axios.interceptors.response.use(data => {
	tryHideFullScreenLoading() //隐藏loading
	return data;
}, err => {
	console.info('err: ' + err);

	tryHideFullScreenLoading() //隐藏loading

	// 没有登录返回登录页
	// if(err.response && err.response.data){
	// 	switch (err.response.status) {
	// 		case 401://token过期或未授权，清除它,清除token信息并跳转到登录页面
	// 			sessionStorage.removeItem("token");
	// 			console.log(router);
	// 			router.replace({
	// 				path: '/login',
	// 				query: {
	// 					redirect: router.currentRoute.fullPath//登录之后跳转到对应页面
	// 				}
	// 			});
	// 			return;
	// 		default:
	// 		   ElementUI.Message.warning(err.response.data);
	// 	}
	// }

	return Promise.reject(err);
});

// lodash
import _ from 'lodash'
Vue.prototype._ = _



//全局loading代码
// let loading        //定义loading变量
// function startLoading() {    //使用Element loading-start 方法
// 	loading = ElementUI.Loading.service({
// 		lock: true,
// 		text: '加载中……',
// 		background: 'rgba(0, 0, 0, 0.5)'
// 	})
// }

let needLoadingRequestCount = 0
export function showFullScreenLoading() {
	if (needLoadingRequestCount === 0) {
		// startLoading() 加载全局loading遮罩
		NProgress.start();
	}
	needLoadingRequestCount++
}

export function tryHideFullScreenLoading() {
	if (needLoadingRequestCount <= 0) return
	needLoadingRequestCount--
	if (needLoadingRequestCount === 0) {
		_.debounce(tryCloseLoading, 300)()
	}
}

const tryCloseLoading = () => {
	if (needLoadingRequestCount === 0) {
		// loading.close() 关闭全局loading遮罩
		NProgress.done()
	}
}





//动态获取路由
let children = [];
let menuTree = require("./assets/json/menu.json");
function acquireRoute(e) {
	for (let i = 0; e.length > i; i++) {
		children.push({
			path: e[i].path, name: e[i].name, component: resolve => require([`${e[i].component}`], resolve)
		});
		if (e[i].childTreeDto && e[i].childTreeDto.length >= 1) {
			acquireRoute(e[i].childTreeDto);
		}
	}
}
acquireRoute(menuTree);

let routes = [
	{ path: '/login', name: '登录', component: resolve => require(['./views/root-login.vue'], resolve) },
	{ path: '/main', name: '主页', component: resolve => require(['./views/root-main.vue'], resolve), children: children }
];

// routes.push({ path: '/book', name: 'book', component: resolve => require(['./views/book.vue'], resolve) });

// vue-router路由
import VueRouter from 'vue-router';
Vue.use(VueRouter);
var router = new VueRouter({ // 将页面组件注册到路由
	routes: routes
})




import Root from './views/root.vue';
new Vue({
	render: function (h) {
		return h(Root);
	},
	router: router,
	store: store
}).$mount('#root');



